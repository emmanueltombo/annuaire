import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactLayoutComponent } from './components/contact-layout/contact-layout.component';
import { RouterModule } from '@angular/router';
import { ContactRoutes } from './contact.routes';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ListContactComponent } from './components/list-contact/list-contact.component';
import { ContactItemComponent } from './components/contact-item/contact-item.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { ContactEditComponent } from './components/contact-edit/contact-edit.component';

@NgModule({
  declarations: [
    ContactLayoutComponent,
    NavbarComponent,
    ListContactComponent,
    ContactItemComponent,
    ContactFormComponent,
    ContactDetailsComponent,
    ContactEditComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ContactRoutes),
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class ContactModule {}
