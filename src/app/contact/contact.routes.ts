import { Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ContactDetailsComponent } from './components/contact-details/contact-details.component';
import { ContactEditComponent } from './components/contact-edit/contact-edit.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ContactLayoutComponent } from './components/contact-layout/contact-layout.component';

export const ContactRoutes: Routes = [
  {
    path: '',
    component: ContactLayoutComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'create',
    component: ContactFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'edit',
    component: ContactEditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':id',
    component: ContactDetailsComponent,
    canActivate: [AuthGuard],
  },
];
