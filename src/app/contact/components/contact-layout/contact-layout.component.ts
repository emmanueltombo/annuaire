import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/core/models/contact.model';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { ContactService } from 'src/app/core/services/contact.service';
@Component({
  selector: 'app-contact-layout',
  templateUrl: './contact-layout.component.html',
  styleUrls: ['./contact-layout.component.css'],
})
export class ContactLayoutComponent implements OnInit {
  contacts: Contact[];
  constructor(
    private contactService: ContactService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    let user: User = this.authService.userData;
    this.contactService.getAll(user.id).subscribe((data) => {
      if (data.status === '200') {
        this.contacts = data.data;
      }
    });
  }

  searchContact(value: string): void {
    let user = this.authService.userData;
    this.contactService.get(value, user.id).subscribe(
      (data) => {
        console.log(data);
        if (data.status === '200') {
          this.contacts = data.data;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
