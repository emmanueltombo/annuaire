import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Contact } from 'src/app/core/models/contact.model';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css'],
})
export class ContactFormComponent implements OnInit {
  formGroup: FormGroup;
  errorMessage: any;
  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      poste: ['', Validators.required],
      telephone: ['', Validators.required],
      email: ['', Validators.required],
      entreprise: ['', Validators.required],
      adresse_entreprise: ['', Validators.required],
    });
    this.formGroup.get('nom').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('prenom').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('poste').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('telephone').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('entreprise').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('telephone').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('adresse_entreprise').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
  }

  createContactEvent() {
    console.log(this.formGroup.value);
    console.log(this.formGroup.status);

    this.errorMessage = {};
    if (this.formGroup.status === 'VALID') {
      this.isLoading = true;
      let user = this.authService.userData;
      let contact: Contact = this.formGroup.value;
      contact.user_id = user.id;

      this.contactService.create(contact).subscribe(
        (data) => {
          console.log(data);

          this.isLoading = false;
          if (data.status === '200') {
            this.router.navigate(['/contact']);
          } else {
            this.errorMessage.text = data.message;
            this.errorMessage.cssClass = 'alert alert-danger';
          }
        },
        (error) => {
          this.isLoading = false;
          this.errorMessage.text = 'Erreur technique';
          this.errorMessage.cssClass = 'alert alert-danger';
        }
      );
    } else {
      this.errorMessage.text = 'Veuillez renseigner tout les champs';
      this.errorMessage.cssClass = 'alert alert-danger';
    }
  }
}
