import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from 'src/app/core/models/contact.model';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.css'],
})
export class ContactItemComponent implements OnInit {
  @Input() contacts: Contact[];
  constructor(private router: Router) {}

  ngOnInit(): void {}

  detailsEvent(contact: Contact): void {
    this.router.navigate(['/contact/' + contact.id], {
      state: { contact: contact },
    });
  }
  trackBy(index, item){
    return item.id;
 }
}
