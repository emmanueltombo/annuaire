import { Component, Input, OnInit } from '@angular/core';
import { Contact } from 'src/app/core/models/contact.model';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'app-list-contact',
  templateUrl: './list-contact.component.html',
  styleUrls: ['./list-contact.component.css'],
})
export class ListContactComponent implements OnInit {
  @Input() contacts: Contact[] = [];
  constructor() {}

  ngOnInit(): void {}
}
