import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  username: any;
  constructor(private authService: AuthService,private router: Router) {}

  ngOnInit(): void {
    let user:any= this.authService.userData;
    this.username=user.username;
    console.log(this.username);
  }
  deconnectEvent(){
    console.log("deconnect");
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
