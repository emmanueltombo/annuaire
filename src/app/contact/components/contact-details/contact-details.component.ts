import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from 'src/app/core/models/contact.model';
import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css'],
})
export class ContactDetailsComponent implements OnInit {
  contact: Contact = new Contact();
  message: string="Suppression...";
  isLoading:boolean=false;
  constructor(private contactService: ContactService, private router: Router) {
    this.contact = this.router.getCurrentNavigation().extras.state.contact;
  }

  ngOnInit(): void {}

  editContact() {
    this.router.navigate(['/contact/edit'], {
      state: { contact: this.contact },
    });
  }
  removeContact() {
    this.message = 'Suppression...';
    this.isLoading=true;
    this.contactService.delete(this.contact.id).subscribe(
      (data) => {
        this.isLoading=false;
        console.log(data);

        if (data.status === '200') {
          this.router.navigate(['/contact']);
        }
      },
      (err) => {
        this.isLoading=false;
        console.log(err);
      }
    );
  }
}
