import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Contact } from 'src/app/core/models/contact.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { ContactService } from 'src/app/core/services/contact.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css'],
})
export class ContactEditComponent implements OnInit {
  formGroup: FormGroup;
  errorMessage: any;
  isLoading: boolean = false;
  contact: Contact;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactService,
    private router: Router,
    private authService: AuthService
  ) {
    this.contact = this.router.getCurrentNavigation().extras.state.contact;
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      poste: ['', Validators.required],
      telephone: ['', Validators.required],
      email: ['', Validators.required],
      entreprise: ['', Validators.required],
      adresse_entreprise: ['', Validators.required],
    });

    this.formGroup.get('nom').setValue(this.contact.nom);
    this.formGroup.get('prenom').setValue(this.contact.prenom);
    this.formGroup.get('poste').setValue(this.contact.poste);
    this.formGroup.get('telephone').setValue(this.contact.telephone);
    this.formGroup.get('email').setValue(this.contact.email);
    this.formGroup.get('entreprise').setValue(this.contact.entreprise);
    this.formGroup.get('adresse_entreprise').setValue(this.contact.adresse_entreprise);

    this.formGroup.get('nom').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('prenom').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('poste').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('telephone').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('email').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('entreprise').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('adresse_entreprise').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
  }

  updateContactEvent() {
    console.log(this.contact);
    console.log(this.formGroup.value);
    console.log(this.formGroup.status);

    this.errorMessage = {};
    if (this.formGroup.status === 'VALID') {
      this.isLoading = true;
      let user=this.authService.userData;
      let data = this.formGroup.value;
      this.contact.nom=data.nom;
      this.contact.prenom=data.prenom;
      this.contact.telephone=data.telephone;
      this.contact.email=data.email;
      this.contact.poste=data.poste;
      this.contact.entreprise=data.entreprise;
      this.contact.adresse_entreprise=data.adresse_entreprise;
      this.contact.user_id=user.id;
      console.log(this.contact);


      this.contactService.update(this.contact).subscribe(
        (data) => {
          console.log(data);

          this.isLoading = false;
          if (data.status === '200') {
            this.router.navigate(['/contact']);
          } else {
            this.errorMessage.text = data.message;
            this.errorMessage.cssClass = 'alert alert-danger';
          }
        },
        (error) => {
          this.isLoading = false;
          this.errorMessage.text = 'Erreur technique';
          this.errorMessage.cssClass = 'alert alert-danger';
        }
      );
    } else {
      this.errorMessage.text = 'Veuillez renseigner tout les champs';
      this.errorMessage.cssClass = 'alert alert-danger';
    }
  }
}
