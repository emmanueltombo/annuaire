import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';
import { HttpDataResponse } from '../models/http-data-response.model';
import { environment } from './../../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ContactService {
  baseUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) {}

  create(contact: Contact) {
    return this.http.post<HttpDataResponse>(this.baseUrl + 'contact', contact);
  }

  getAll(user_id: number) {
    if (user_id === undefined) throw new Error('invalide id utilisateur');
    return this.http.get<HttpDataResponse>(this.baseUrl + 'contact/' + user_id);
  }

  get(contact_desc, user_id: number) {
    return this.http.get<HttpDataResponse>(
      this.baseUrl + 'contact/' + user_id + '/' + contact_desc
    );
  }

  update(contact: Contact) {
    return this.http.put<HttpDataResponse>(this.baseUrl + 'contact', contact);
  }

  delete(contact_id: number) {
    return this.http.delete<HttpDataResponse>(
      this.baseUrl + 'contact/' + contact_id
    );
  }
}
