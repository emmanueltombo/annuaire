import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Contact } from '../models/contact.model';
import { HttpDataResponse } from '../models/http-data-response.model';
import { environment } from './../../../environments/environment';

const baseUrl = environment.apiBaseUrl;

import { ContactService } from './contact.service';

describe('ContactService', () => {
  let service: ContactService,
    httpTestingController: HttpTestingController,
    dataExpected: HttpDataResponse;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ContactService],
    });
    service = TestBed.inject(ContactService);
    httpTestingController = TestBed.inject(HttpTestingController);
    dataExpected = new HttpDataResponse();
    dataExpected.status = '200';
    dataExpected.message = 'ok';
  });

  it('should return contact created', () => {
    let contact = new Contact();
    contact.id = 1;
    contact.nom = 'Lutumba';
    contact.prenom = 'Emmanuel';
    contact.telephone = '+243822748670';
    contact.email = 'emmanuelutumba77@gmail.com';
    contact.entreprise = 'CREAS MIT';
    contact.adresse_entreprise = 'Colonel Ebeya N°3';
    contact.poste = 'Lead Tech';
    dataExpected.data = contact;

    let dataAvailable: HttpDataResponse;
    service.create(contact).subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      url: baseUrl+'contact',
      method: 'POST',
    });
    req.flush(dataExpected);
    expect(dataAvailable).toEqual(dataExpected);
  });

  it('should return contact by id', () => {
    let contact = new Contact();
    contact.id = 1;
    contact.nom = 'Lutumba';
    contact.prenom = 'Emmanuel';
    contact.telephone = '+243822748670';
    contact.email = 'emmanuelutumba77@gmail.com';
    contact.entreprise = 'CREAS MIT';
    contact.adresse_entreprise = 'Colonel Ebeya N°3';
    contact.poste = 'Lead Tech';
    dataExpected.data = contact;

    let dataAvailable: HttpDataResponse;
    service.get(2, 1).subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      url: baseUrl + 'contact/1/2',
      method: 'GET',
    });
    req.flush(dataExpected);
    expect(dataAvailable).toEqual(dataExpected);
  });

  it('should return contacts', () => {
    let contact1 = new Contact();
    contact1.id = 1;
    contact1.nom = 'Lutumba';
    contact1.prenom = 'Emmanuel';
    contact1.telephone = '+243822748670';
    contact1.email = 'emmanuelutumba77@gmail.com';
    contact1.entreprise = 'CREAS MIT';
    contact1.adresse_entreprise = 'Colonel Ebeya N°3';
    contact1.poste = 'Lead Tech';

    let contact2 = new Contact();
    contact2.id = 1;
    contact2.nom = 'Lutumba';
    contact2.prenom = 'Emmanuel';
    contact2.telephone = '+243822748670';
    contact2.email = 'emmanuelutumba77@gmail.com';
    contact2.entreprise = 'CREAS MIT';
    contact2.adresse_entreprise = 'Colonel Ebeya N°3';
    contact2.poste = 'Lead Tech';

    let contacts = [];
    contacts.push(contact1);
    contacts.push(contact2);

    dataExpected.data = contacts;

    let dataAvailable: HttpDataResponse;
    service.getAll(1).subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      url: baseUrl + 'contact/1',
      method: 'GET',
    });
    req.flush(dataExpected);
    expect(dataAvailable).toEqual(dataExpected);
  });

  it('should return contact updated', () => {
    let contact = new Contact();
    contact.id = 1;
    contact.nom = 'Lutumba';
    contact.prenom = 'Emmanuel';
    contact.telephone = '+243822748670';
    contact.email = 'emmanuelutumba77@gmail.com';
    contact.entreprise = 'CREAS MIT';
    contact.adresse_entreprise = 'Colonel Ebeya N°3';
    contact.poste = 'Lead Tech';
    dataExpected.data = contact;

    let dataAvailable: HttpDataResponse;
    service.update(contact).subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      url: baseUrl+'contact',
      method: 'PUT',
    });
    req.flush(dataExpected);
    expect(dataAvailable).toEqual(dataExpected);
  });

  it('should return contact deleted', () => {
    let contact = new Contact();
    contact.id = 1;
    contact.nom = 'Lutumba';
    contact.prenom = 'Emmanuel';
    contact.telephone = '+243822748670';
    contact.email = 'emmanuelutumba77@gmail.com';
    contact.entreprise = 'CREAS MIT';
    contact.adresse_entreprise = 'Colonel Ebeya N°3';
    contact.poste = 'Lead Tech';
    dataExpected.data = contact;

    let dataAvailable: HttpDataResponse;
    service.delete(1).subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      url: baseUrl + 'contact/1',
      method: 'DELETE',
    });
    req.flush(dataExpected);
    expect(dataAvailable).toEqual(dataExpected);
  });
});
