import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConfigService } from 'src/app/config/app-config.service';
import { HttpDataResponse } from '../models/http-data-response.model';
import { User } from '../models/user.model';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends AppConfigService {
  baseUrl = environment.apiBaseUrl;

  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private http: HttpClient) {
    super();
    this.userSubject=new BehaviorSubject<User>(JSON.parse(localStorage.getItem("user")));
  }

  public get userData(): User {
    return this.userSubject.value;
  }

  logout(){
    localStorage.removeItem("user");
  }

  login(username: string, password: string) {
    return this.http
      .post<HttpDataResponse>(this.baseUrl + 'auth/login', {
        username,
        password,
      })
      .pipe(
        map((data) => {
          if (data.status === '200') {
            this.userSubject.next(data.data);
            localStorage.setItem('user', JSON.stringify(data.data));
            localStorage.setItem('token', data.token);
          }
          return data;
        })
      );
  }

  signup(userProfile: User) {
    return this.http.post<HttpDataResponse>(
      this.baseUrl + 'auth/signup',
      userProfile
    );
  }
  update(userProfile: User) {
    return this.http.put<HttpDataResponse>(
      this.baseUrl + 'auth',
      userProfile
    );
  }
}
