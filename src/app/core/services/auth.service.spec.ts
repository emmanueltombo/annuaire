import { TestBed } from '@angular/core/testing';
import { HttpDataResponse } from '../models/http-data-response.model';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from './../../../environments/environment';


import { AuthService } from './auth.service';
import { User } from '../models/user.model';
const baseUrl = environment.apiBaseUrl;

describe('AuthService', () => {
  let service: AuthService,
    httpTestingController: HttpTestingController,
    dataExpected: HttpDataResponse;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService],
    });
    service = TestBed.inject(AuthService);
    httpTestingController = TestBed.inject(HttpTestingController);
    dataExpected = new HttpDataResponse();
    dataExpected.status = '200';
    dataExpected.message = 'ok';
  });

  it('should return httpDataResponse after logged', () => {
    dataExpected.token = 'token';
    dataExpected.data = { id: 1, username: 'emma' };

    let dataAvailable: HttpDataResponse;
    service.login('emma', '123').subscribe((data) => {
      dataAvailable = data;
    });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: baseUrl + 'auth/login',
    });
    req.flush(dataExpected);

    expect(dataAvailable).toEqual(dataExpected);
  });

  it('should return httpDataResponse with userProfil data', () => {
    let userProfil: User = new User();
    userProfil.firstname='Emmanuel';
    userProfil.lastname='LUTUMBA';
    userProfil.username='emma';
    userProfil.password='emma';

    let dataAvailable: HttpDataResponse;
    service
      .signup(userProfil)
      .subscribe((data) => {
        dataAvailable = data;
      });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: baseUrl + 'auth/signup',
    });
    req.flush(dataExpected);

    expect(dataAvailable).toEqual(dataExpected);
  });
});
