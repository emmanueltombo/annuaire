export class HttpDataResponse {
  public status: string;
  public message: string;
  public token: string;
  public data: any;
}
