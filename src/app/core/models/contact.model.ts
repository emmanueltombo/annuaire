export class Contact {
  id: number;
  nom: string;
  prenom: string;
  email: string;
  telephone: string;
  poste: string;
  entreprise: string;
  adresse_entreprise: string;
  user_id:number
}
