import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.css'],
})
export class LoginLayoutComponent implements OnInit {
  formGroup: FormGroup;
  errorMessage: any;
  isLoading: boolean = false;
  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onLoginEvent() {
    this.errorMessage = {};
    if (this.formGroup.status === 'VALID') {
      this.isLoading = true;
      let username = this.formGroup.value.username;
      let password = this.formGroup.value.password;

      this.authService.login(username, password).subscribe(
        (data) => {
          this.isLoading = false;
          console.log(data);
          if (data.status == '200') {
            this.errorMessage = {};
            this.router.navigate(['/contact/']);
          } else {
            this.errorMessage.text = data.message;
            this.errorMessage.cssClass = 'alert alert-danger';
          }
        },
        (err) => {
          this.isLoading = false;
          this.errorMessage.text = 'Erreur technique';
          this.errorMessage.cssClass = 'alert alert-danger';
          console.log(err);
        }
      );
    }
  }
}
