import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
})
export class UserEditComponent implements OnInit {
  formGroup: FormGroup;
  errorMessage: any;
  isLoading: boolean = false;
  user: User;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.user = this.authService.userData;
    this.formGroup = this.formBuilder.group({
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      username: ['', Validators.required],
      password: [''],
    });

    this.formGroup.get('lastname').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('firstname').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('username').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('password').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });

    this.formGroup.get('lastname').setValue(this.user.lastname);
    this.formGroup.get('firstname').setValue(this.user.firstname);
    this.formGroup.get('username').setValue(this.user.username);
    this.formGroup.get('password').setValue(this.user.password);
  }

  updateUserEvent() {
    console.log('click singup btn');
    this.formGroup.get('password').clearValidators();
    this.formGroup.get('password').updateValueAndValidity();

    this.errorMessage = {};
    if (this.formGroup.status === 'VALID') {
      this.isLoading = true;
      this.user.firstname = this.formGroup.value.firstname;
      this.user.lastname = this.formGroup.value.lastname;
      this.user.username = this.formGroup.value.username;
      this.user.password = this.formGroup.value.password;
      this.authService.update(this.user).subscribe((data) => {
        this.isLoading = false;
        if (data.status === '200') {
          this.router.navigate(['/login']);
        } else {
          this.errorMessage.text = data.message;
          this.errorMessage.cssClass = 'alert alert-danger';
        }
      });
    } else {
      this.errorMessage.text = 'Veuillez renseigner tout les champs';
      this.errorMessage.cssClass = 'alert alert-danger';
    }
  }
}
