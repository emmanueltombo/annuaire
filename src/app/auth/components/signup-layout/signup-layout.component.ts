import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-signup-layout',
  templateUrl: './signup-layout.component.html',
  styleUrls: ['./signup-layout.component.css'],
})
export class SignupLayoutComponent implements OnInit {
  formGroup: FormGroup;
  errorMessage: any;
  isLoading: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.formGroup.get('lastname').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('firstname').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('username').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
    this.formGroup.get('password').valueChanges.subscribe((data) => {
      this.errorMessage = {};
    });
  }

  singupEvent() {
    console.log('click singup btn');
    console.log(this.formGroup.value);
    console.log(this.formGroup.status);


    this.errorMessage = {};
    if (this.formGroup.status === 'VALID') {
      this.isLoading = true;
      let user: User = this.formGroup.value;
      this.authService.signup(user).subscribe((data) => {
        this.isLoading = false;
        if (data.status === '200') {
          this.router.navigate(['/login']);
        } else {
          this.errorMessage.text = data.message;
          this.errorMessage.cssClass = 'alert alert-danger';
        }
      });
    } else {
      this.errorMessage.text = 'Veuillez renseigner tout les champs';
      this.errorMessage.cssClass = 'alert alert-danger';
    }
  }
}
