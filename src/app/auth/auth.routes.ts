import { Router, Routes } from '@angular/router';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { SignupLayoutComponent } from './components/signup-layout/signup-layout.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
export const AuthRoutes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
  },
  {
    path: 'login',
    component: LoginLayoutComponent,
  },
  {
    path: 'signup',
    component: SignupLayoutComponent,
  },
  {
    path: 'user/edit',
    component: UserEditComponent,
  },
];
