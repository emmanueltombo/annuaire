import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthRoutes } from './auth.routes';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupLayoutComponent } from './components/signup-layout/signup-layout.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
@NgModule({
  declarations: [LoginLayoutComponent, SignupLayoutComponent, UserEditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AuthRoutes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AuthModule {}
