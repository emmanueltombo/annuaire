import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppConfigService {
  public appConfig = { apiBaseUrl: 'http://localhost:8081/annuaire' };
}
